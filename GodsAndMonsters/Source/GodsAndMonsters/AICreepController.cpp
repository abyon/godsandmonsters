// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "CreepCharacter.h"
#include "AICreepController.h"
#include "GodsAndMonstersGameMode.h"
#include "FPSCharacterController.h"

AAICreepController::AAICreepController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
    //deets
    WeaponRange = 150.0f;
    
}


void AAICreepController::BeginPlay()
{
    //Supers original begin play
    Super::BeginPlay();
    //Set state to beginning
    CurrentState = Start;
    
    
}

void AAICreepController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
    APawn* CharacterLocation = NULL;
    AGodsAndMonstersGameMode* GM = GetWorld()->GetAuthGameMode<AGodsAndMonstersGameMode>();
    if(GM){
        TArray<AFPSCharacterController*> MonsterList = GM->MonsterList;
        for(AFPSCharacterController* MonsterController : MonsterList){
            ACreepCharacter* CreepCharacter = Cast<ACreepCharacter>(GetPawn());
            if(CreepCharacter){
                if(MonsterController->GetTeamNum() != CreepCharacter->TeamNum){
                    CharacterLocation = MonsterController->GetPawn();
                }
            }
        }
    }
    
    switch(CurrentState)
    {
        case Start:
            CurrentState = Chase;
            break;
        case Chase:
            MoveToActor(CharacterLocation);
            break;
        case Attack:
        {
            ACreepCharacter* CreepCharacter = Cast<ACreepCharacter>(GetPawn());
            if (!CreepCharacter->isDead) {
                if (CharacterLocation) {
                
                    if (!(FVector::Dist(GetCharacter()->GetActorLocation(), CharacterLocation->GetActorLocation()) < WeaponRange))
                    {
                        CurrentState = Chase;
                        Cast<ACreepCharacter>(GetCharacter())->StopAttack();
                    }
                }
            }
        }
            break;
        case Dead:
            Destroy();
            break;
        default:
            break;
    }
}

void AAICreepController::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
    CurrentState = Attack;
    
    ACreepCharacter* CreepCharVar = Cast<ACreepCharacter>(GetCharacter());
    if (CreepCharVar)
    {
        CreepCharVar->StartAttack();
    }
}




