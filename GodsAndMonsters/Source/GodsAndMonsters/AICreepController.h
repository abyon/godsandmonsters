// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIEnemyController.h"
#include "AICreepController.generated.h"

/**
 * 
 */
UCLASS()
class GODSANDMONSTERS_API AAICreepController : public AAIEnemyController
{
    GENERATED_BODY()
    
public:
    AAICreepController(const FObjectInitializer& ObjectInitializer);
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)
    float WeaponRange;
    
    //Oh the overrides
    void BeginPlay() override;
    void Tick(float DeltaTime) override;
    void OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) override;
    
    enum StateOfMind{
        Start,
        Chase,
        Attack,
        Dead
    };
    
    StateOfMind CurrentState;
    
	
	
	
	
};
