// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "TowerPawn.h"
#include "AITowerController.h"
#include "GodsAndMonstersGameMode.h"
#include "FPSCharacterController.h"

AAITowerController::AAITowerController()
{
    //deets
    WeaponRange = 1100.0f;
    
}


void AAITowerController::BeginPlay()
{
    //Supers original begin play
    Super::BeginPlay();
    //Set state to beginning
    CurrentState = Start;
    isFiring = false;
    
    
}

void AAITowerController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
    APawn* CharacterLocation = NULL;
    AGodsAndMonstersGameMode* GM = GetWorld()->GetAuthGameMode<AGodsAndMonstersGameMode>();
    if(GM){
        TArray<AFPSCharacterController*> MonsterList = GM->MonsterList;
        for(AFPSCharacterController* MonsterController : MonsterList){
            APlaceable* Tower = Cast<APlaceable>(GetPawn());
            if(Tower){
                if(MonsterController->GetTeamNum() != Tower->TeamNum){
                    CharacterLocation = MonsterController->GetPawn();
                }
            }
        }
    }

    
    if(CharacterLocation){
    
    switch(CurrentState)
    {
        case Start:
        {
            if(FVector::Dist(GetPawn()->GetActorLocation(), CharacterLocation->GetActorLocation()) < WeaponRange)
            {
                CurrentState = Attack;
            }
            break;
        }
        case Attack:
        {
            ATowerPawn* TowerPawnVar = Cast<ATowerPawn>(GetPawn());
            if (TowerPawnVar)
            {
                TowerPawnVar->RotateToFace();
                //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Start AttackMode"));
                if(!isFiring){
                TowerPawnVar->StartAttack();
                    isFiring = true;
                }
            }
            
            
            if (!(FVector::Dist(GetPawn()->GetActorLocation(), CharacterLocation->GetActorLocation()) < WeaponRange))
            {
                //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Stop AttackMode"));
                CurrentState = Start;
                TowerPawnVar->StopAttack();
                isFiring = false;
            }
            break;
        }
        case Dead:
        {
            Destroy();
            break;
        }
        default:
        {
            break;
        }
    }
    }
}

/*void AAICreepController::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
    CurrentState = Attack;
    
    ACreepCharacter* CreepCharVar = Cast<ACreepCharacter>(GetCharacter());
    if (CreepCharVar)
    {
        CreepCharVar->StartAttack();
    }
}*/




