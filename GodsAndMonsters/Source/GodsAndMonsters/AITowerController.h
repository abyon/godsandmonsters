// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIEnemyController.h"
#include "AITowerController.generated.h"

/**
 * 
 */
UCLASS()
class GODSANDMONSTERS_API AAITowerController : public AAIEnemyController
{
	GENERATED_BODY()
    
public:
	
    AAITowerController();
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)
    float WeaponRange;
    
    //Oh the overrides
    void BeginPlay() override;
    void Tick(float DeltaTime) override;
    //void OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) override;
    
    enum StateOfMind{
        Start,
        Attack,
        Dead
    };
    
    bool isFiring;
    StateOfMind CurrentState;
    
	
	
	
};
