// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "Barrier.h"


// Sets default values
ABarrier::ABarrier()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    
    HP = 25.0f;
    
    bReplicates = true;
    bReplicateMovement = true;
    
    CurrentFacing = FVector::ZeroVector;
    
    GunOffset = FVector(10.f, 0.f, 200.f);
    
    // The pickup is valid when it is created.
    bIsActive = true;
    
    SetActorEnableCollision(true);
    
    BaseDummyComponent = CreateDefaultSubobject<USceneComponent>(TEXT("BaseDummyComponent"));
    RootComponent = BaseDummyComponent;
    
    // Create the root SphereComponent to handle the pickup's collision
    BaseCollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BaseBoxComponent"));
    BaseCollisionComponent->InitBoxExtent(FVector(10.f, 10.f, 20.f));
    BaseCollisionComponent->SetRelativeLocation(FVector(0.f, 0.f, 200.f));
    BaseCollisionComponent->BodyInstance.SetCollisionProfileName("Tower");
    BaseCollisionComponent->AttachTo(RootComponent);
    
    // Set the SphereComponent as the root component.
    //RootComponent = BaseCollisionComponent;
}

// Called when the game starts or when spawned
void ABarrier::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void ABarrier::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );
    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::red, TEXT("THING"));
    
}

// Called to bind functionality to input
void ABarrier::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
    Super::SetupPlayerInputComponent(InputComponent);
    
}

float ABarrier::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
                             AController* EventInstigator, AActor* DamageCauser)
{
    float ActualDamage = Super::TakeDamage(Damage, DamageEvent,
                                           EventInstigator, DamageCauser);
    if (ActualDamage > 0.0f)
    {
        HP -= ActualDamage;
        if (HP <= 0.0f)
        {
            HP = 0.0f;
            // We're dead
            bCanBeDamaged = false; // Don't allow further damage
            
            // TODO: Process death
            DeadTower();
            
        }
    }
    return ActualDamage;
}

bool ABarrier::DeadTower_Validate() { return true; }
void ABarrier::DeadTower_Implementation()
{
    
    if (Role == ROLE_Authority)
    {
        if(Controller) {
            if(PointsPickupClass != NULL) {
                const FRotator SpawnRotation = GetActorRotation();
                const FVector shiftVector = FVector(0, 0, 70.0f);
                const FVector SpawnLocation = GetActorLocation() + shiftVector;
                FActorSpawnParameters params;
                params.Owner = this;
                params.Instigator = Instigator;
                UWorld* const World = GetWorld();
                if(World != NULL){
                    // spawn the pickup at dead creep
                    APointsPickup* NewPoints = World->SpawnActor<APointsPickup>(PointsPickupClass, SpawnLocation, SpawnRotation, params);
                    if(NewPoints){
                        NewPoints->SetReplicates(true);
                    }
                }
            }
        }
        
        GetWorldTimerManager().SetTimer(this, &ABarrier::DeadTower, 0.25f, true);
        Controller->UnPossess();
        Destroy();
    }
}


void ABarrier::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>&
                                            OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    
    DOREPLIFETIME( ABarrier, HP);
    DOREPLIFETIME( ABarrier, CurrentFacing )
}
