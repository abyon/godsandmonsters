// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "AICreepController.h"
#include "FirstPersonCharacter.h"
#include "CreepCharacter.h"

ACreepCharacter::ACreepCharacter()
{
    AIControllerClass = AAICreepController::StaticClass();
    HP = 40.0f;
    HammerDamage = 5.0f;
    
    isDead = false;
    bReplicates = true;
    bReplicateMovement = true;
    
}

bool ACreepCharacter::StartAttack_Validate() { return true; }
void ACreepCharacter::StartAttack_Implementation()
{
    float HammerSwingDur = PlayAnimMontage(AttackAnim);
    
    if (Role == ROLE_Authority)
    {
        GetWorldTimerManager().SetTimer(this, &ACreepCharacter::HurtPlayer, HammerSwingDur, true);
    }
}

bool ACreepCharacter::StopAttack_Validate() { return true; }
void ACreepCharacter::StopAttack_Implementation()
{
    StopAnimMontage(AttackAnim);
    if (Role == ROLE_Authority)
    {
        GetWorldTimerManager().ClearTimer(this, &ACreepCharacter::HurtPlayer);
    }
}

float ACreepCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
                                  AController* EventInstigator, AActor* DamageCauser)
{
    float ActualDamage = Super::TakeDamage(Damage, DamageEvent,
                                           EventInstigator, DamageCauser);
    if (ActualDamage > 0.0f)
    {
        HP -= ActualDamage;
        // UE_LOG(LogTemp, Warning, TEXT("Creep Was hit"));
        if (HP <= 0.0f)
        {
            HP = 0.0f;
            // We're dead
            bCanBeDamaged = false; // Don't allow further damage
            
            // TODO: Process death
            
            StopAttack();
            DeadCreep();
            
        }
    }
    return ActualDamage;
}

bool ACreepCharacter::DeadCreep_Validate() { return true; }
void ACreepCharacter::DeadCreep_Implementation()
{
    if (Role == ROLE_Authority)
    {
        if (!isDead) {
            isDead = true;
            if(Controller){
                if(PointsPickupClass != NULL) {
                    const FRotator SpawnRotation = GetActorRotation();
                    const FVector SpawnLocation = GetActorLocation();
                    FActorSpawnParameters params;
                    params.Owner = this;
                    params.Instigator = Instigator;
                    UWorld* const World = GetWorld();
                    if(World != NULL){
                        // spawn the pickup at dead creep
                        APointsPickup* NewPoints = World->SpawnActor<APointsPickup>(PointsPickupClass, SpawnLocation, SpawnRotation, params);
                        if(NewPoints){
                            NewPoints->SetReplicates(true);
                        }
                    }
                }
            }
        }
        
        StopAnimMontage();
        float DeathDur = PlayAnimMontage(DeathAnim);
        
        AAICreepController* CreepAI = Cast<AAICreepController>(GetController());
        if (CreepAI) {
            CreepAI->CurrentState = CreepAI->Dead;
        }
    
        GetWorldTimerManager().SetTimer(this, &ACreepCharacter::DeadCreep, DeathDur - 0.25f, true);
        Controller->UnPossess();
        Destroy();
    }
    
    
}

void ACreepCharacter::HurtPlayer()
{
//    UGameplayStatics::GetPlayerPawn(this, 0)->TakeDamage(HammerDamage, FDamageEvent(), GetInstigatorController(), this);
    
    AFirstPersonCharacter* MyCharacter = Cast<AFirstPersonCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 1));
    
    if (MyCharacter) {
        MyCharacter->TakeDamage(HammerDamage, FDamageEvent(), GetInstigatorController(), this);
    }
}

void ACreepCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>&
                                                       OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    
    DOREPLIFETIME( ACreepCharacter, HP);
}