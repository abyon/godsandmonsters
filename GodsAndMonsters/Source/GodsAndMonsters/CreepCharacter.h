// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "EnemyCharacter.h"
#include "PointsPickup.h"
#include "CreepCharacter.generated.h"

/**
 * 
 */
UCLASS()
class GODSANDMONSTERS_API ACreepCharacter : public AEnemyCharacter
{
    GENERATED_BODY()
    
    //UPROPERTY( replicated )
    //ACreepCharacter * Self;
    
public:
    ACreepCharacter();
    
    
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    UAnimMontage* AttackAnim;
    
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    UAnimMontage* DeathAnim;
    
    UFUNCTION(Reliable, NetMulticast, WithValidation)
    void StartAttack();
    bool StartAttack_Validate();
    void StartAttack_Implementation();
    
    UFUNCTION(Reliable, NetMulticast, WithValidation)
    void StopAttack();
    bool StopAttack_Validate();
    void StopAttack_Implementation();
    
    UFUNCTION(Reliable, NetMulticast, WithValidation)
    void DeadCreep();
    bool DeadCreep_Validate();
    void DeadCreep_Implementation();
    
    bool isDead;
    
    void HurtPlayer();
    
    uint32 TeamNum;
    
    float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
    
    UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category=Health)
    float HP;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)
    float HammerDamage;
	
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;
    
    /** Projectile class to spawn */
    UPROPERTY(EditDefaultsOnly, Category = PointsClass)
    TSubclassOf<class APointsPickup> PointsPickupClass;

};
