#include "GodsAndMonsters.h"
#include "AITowerController2.h"
#include "CreepFactory.h"
#include "GodsAndMonstersGameMode.h"
#include "FPSCharacterController.h"

// Sets default values
ACreepFactory::ACreepFactory()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    
    AIControllerClass = AAITowerController2::StaticClass();
    HP = 40.0f;
    TowerDamage = 10.0f;
    
    bReplicates = true;
    bReplicateMovement = true;
    
    CurrentFacing = FVector::ZeroVector;
    
    GunOffset = FVector(100.f, 30.f, 10.f);
    
    // The pickup is valid when it is created.
    bIsActive = true;
    
    SetActorEnableCollision(true);
    
    BaseDummyComponent = CreateDefaultSubobject<USceneComponent>(TEXT("BaseDummyComponent"));
    RootComponent = BaseDummyComponent;
    
    // Create the root SphereComponent to handle the pickup's collision
    BaseCollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BaseBoxComponent"));
    BaseCollisionComponent->InitBoxExtent(FVector(10.f, 10.f, 20.f));
    BaseCollisionComponent->SetRelativeLocation(FVector(0.f, 0.f, 200.f));
    BaseCollisionComponent->BodyInstance.SetCollisionProfileName("Tower");
    BaseCollisionComponent->AttachTo(RootComponent);
    
}

// Called when the game starts or when spawned
void ACreepFactory::BeginPlay()
{
    Super::BeginPlay();
    
}

// Called every frame
void ACreepFactory::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );
    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::red, TEXT("THING"));
    
}

// Called to bind functionality to input
void ACreepFactory::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
    Super::SetupPlayerInputComponent(InputComponent);
    
}

bool ACreepFactory::StartAttack_Validate() { return true; }
void ACreepFactory::StartAttack_Implementation()
{
    
    //if (Role == ROLE_Authority)
    //{
    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Attack Timer On"));
    GetWorldTimerManager().SetTimer(this, &ACreepFactory::FireAtMonster, FiringRate, true);
    //}
}

bool ACreepFactory::StopAttack_Validate() { return true; }
void ACreepFactory::StopAttack_Implementation()
{
    if (Role == ROLE_Authority)
    {
        //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Attack Timer Off"));
        GetWorldTimerManager().ClearTimer(this, &ACreepFactory::FireAtMonster);
    }
}

float ACreepFactory::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
                             AController* EventInstigator, AActor* DamageCauser)
{
    float ActualDamage = Super::TakeDamage(Damage, DamageEvent,
                                           EventInstigator, DamageCauser);
    if (ActualDamage > 0.0f)
    {
         GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Hit Tower 2"));
        HP -= ActualDamage;
        if (HP <= 0.0f)
        {
            HP = 0.0f;
            // We're dead
            bCanBeDamaged = false; // Don't allow further damage
            
            // TODO: Process death
            DeadTower();
            
        }
    }
    return ActualDamage;
}

bool ACreepFactory::DeadTower_Validate() { return true; }
void ACreepFactory::DeadTower_Implementation()
{
    if (Role == ROLE_Authority)
    {
        if(Controller) {
            if(PointsPickupClass != NULL) {
                const FRotator SpawnRotation = GetActorRotation();
                const FVector SpawnLocation = GetActorLocation();
                FActorSpawnParameters params;
                params.Owner = this;
                params.Instigator = Instigator;
                UWorld* const World = GetWorld();
                if(World != NULL){
                    // spawn the pickup at dead creep
                    APointsPickup* NewPoints = World->SpawnActor<APointsPickup>(PointsPickupClass, SpawnLocation, SpawnRotation, params);
                    if(NewPoints){
                        NewPoints->SetReplicates(true);
                    }
                }
            }
        }
        
        AAITowerController2* TowerAI = Cast<AAITowerController2>(GetController());
        if (TowerAI) {
            TowerAI->CurrentState = TowerAI->Dead;
        }
        
        platform->Occupied = false;
        
        GetWorldTimerManager().SetTimer(this, &ACreepFactory::DeadTower, 0.25f, true);
        Controller->UnPossess();
        Destroy();
    }
}

void ACreepFactory::HurtPlayer()
{
    UGameplayStatics::GetPlayerPawn(this, 0)->TakeDamage(TowerDamage, FDamageEvent(), GetInstigatorController(), this);
}

bool ACreepFactory::RotateToFace_Validate() { return true; }
void ACreepFactory::RotateToFace_Implementation()
{
    
    APawn* CharacterLocation = NULL;
    AGodsAndMonstersGameMode* GM = GetWorld()->GetAuthGameMode<AGodsAndMonstersGameMode>();
    if(GM){
        TArray<AFPSCharacterController*> MonsterList = GM->MonsterList;
        for(AFPSCharacterController* MonsterController : MonsterList){
            if(MonsterController->GetTeamNum() != TeamNum){
                CharacterLocation = MonsterController->GetPawn();
            }
        }
    }
    
    if(CharacterLocation){
        FVector PointOfFocus = CharacterLocation->GetActorLocation();
        
        FVector NewFacing = (PointOfFocus - GetActorLocation());
        NewFacing = FVector(NewFacing.X, NewFacing.Y, 0);
        NewFacing.Normalize();
        
        FRotator NewRotation = FRotator(0, NewFacing.Rotation().Yaw, 0);
        
        SetActorRotation(NewRotation);
        
        CurrentFacing = NewRotation.Vector();
    }
}

bool ACreepFactory::FireAtMonster_Validate(){ return true; }
void ACreepFactory::FireAtMonster_Implementation()
{
    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("FireAtMonster Called."));
    // [Server] try and fire a projectile
    if(Role == ROLE_Authority){
        if(ProjectileClass != NULL) {
            const FRotator SpawnRotation = GetControlRotation();
            // MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
            const FVector SpawnLocation = GetActorLocation() + SpawnRotation.RotateVector(GunOffset);
            FActorSpawnParameters params;
            params.Owner = this;
            params.Instigator = Instigator;
            UWorld* const World = GetWorld();
            if(World != NULL){
                // spawn the projectile at the muzzle
                ACreepCharacter* NewProjectile = World->SpawnActor<ACreepCharacter>(ProjectileClass, SpawnLocation, SpawnRotation, params);
                if(!NewProjectile){
                    return;
                }
                NewProjectile->TeamNum = TeamNum;
                NewProjectile->SetReplicates(true);
            }
        }
    }
    
    
}

void ACreepFactory::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>&
                                            OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    
    DOREPLIFETIME( ACreepFactory, HP);
    DOREPLIFETIME( ACreepFactory, CurrentFacing )
}

