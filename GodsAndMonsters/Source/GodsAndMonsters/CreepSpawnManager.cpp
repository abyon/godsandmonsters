// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "CreepCharacter.h"
#include "CreepSpawnManager.h"

ACreepSpawnManager::ACreepSpawnManager()
{
    MaxSpawnTime = 10.0f;
    MinSpawnTime = 4.5f;
    
}

void ACreepSpawnManager::BeginPlay()
{
    if (Role == ROLE_Authority)
    {
    GetWorldTimerManager().SetTimer(this, &ACreepSpawnManager::SpawnCreep, MinSpawnTime, true);
    RandomSpawnDelay = GetRandomSpawnDelay();
    }
}

bool ACreepSpawnManager::SpawnCreep_Validate(){ return true;}
void ACreepSpawnManager::SpawnCreep_Implementation()
{
    
    GetWorldTimerManager().ClearTimer(this, &ACreepSpawnManager::SpawnCreep);
    
    if(SpawnedCreep)
    {
        UWorld* const World = GetWorld();
        
        if(World)
        {
            int RandomLocation = FMath::FRandRange(0, 2);
            FActorSpawnParameters SpawnParameters;
            SpawnParameters.Owner = this;
            SpawnParameters.Instigator = Instigator;
            
            FRotator SpawnRotation;
            SpawnRotation.Yaw = 0.0f;
            SpawnRotation.Pitch = 0.0f;
            SpawnRotation.Roll = 0.0f;
            
            
            ACreepCharacter* NewestCreep = World->SpawnActor<ACreepCharacter>(SpawnedCreep, SpawnArray[RandomLocation]->GetActorLocation(), SpawnRotation, SpawnParameters);
            
            if (NewestCreep)
            {
                NewestCreep->SpawnDefaultController();
            }
        }
    }
    
    RandomSpawnDelay = GetRandomSpawnDelay();
    //Set the timer for the next dwarf's arrival/spawn.
    GetWorldTimerManager().SetTimer(this, &ACreepSpawnManager::SpawnCreep, RandomSpawnDelay, true);
}

float ACreepSpawnManager::GetRandomSpawnDelay()
{
    float RSD = FMath::FRandRange(MinSpawnTime, MaxSpawnTime);
    return RSD;
}
