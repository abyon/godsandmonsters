// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "CreepSpawnManager.generated.h"

/**
 *
 */
UCLASS()
class GODSANDMONSTERS_API ACreepSpawnManager : public AActor
{
    GENERATED_BODY()
public:
    
    ACreepSpawnManager();
    
    UPROPERTY(EditAnywhere, Category = Spawn)
    TSubclassOf<ACharacter> SpawnedCreep;
    
    UPROPERTY(EditAnywhere, Category = Spawn)
    TArray<ATargetPoint*> SpawnArray;
    
    UPROPERTY(EditAnywhere, Category = Spawn)
    float MaxSpawnTime;
    
    UPROPERTY(EditAnywhere, Category = Spawn)
    float MinSpawnTime;
    
    UFUNCTION(Reliable, Server, WithValidation)
    void SpawnCreep();
    bool SpawnCreep_Validate();
    void SpawnCreep_Implementation();
    
    void BeginPlay() override;
    
private:
    
    float GetRandomSpawnDelay();
    float RandomSpawnDelay;
    
    
};
