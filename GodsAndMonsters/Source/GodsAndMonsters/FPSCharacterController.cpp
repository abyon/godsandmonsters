// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "FPSCharacterController.h"
#include "GodPlayerController.h"
#include "GodsAndMonstersGameMode.h"

// Constructor
AFPSCharacterController::AFPSCharacterController(const FObjectInitializer& ObjectInitializer)
    :Super(ObjectInitializer)
{
    // Set our turn rates for input
    BaseTurnRate = 45.f;
    BaseLookUpRate = 45.f;
    
    // Set offset for 'muzzle' of gun
    GunOffset = FVector(100.f, 30.f, 10.f);
}

// Input

void AFPSCharacterController::SetupInputComponent(){
    Super::SetupInputComponent();
    
    // Set up gameplay key bindings
    InputComponent->BindAction("Jump", IE_Pressed, this, &AFPSCharacterController::MakeJump);
    InputComponent->BindAction("Jump", IE_Released, this, &AFPSCharacterController::MakeStopJump);
    InputComponent->BindAction("Fire", IE_Pressed, this, &AFPSCharacterController::OnFire);
    
    InputComponent->BindAxis("MoveForward", this, &AFPSCharacterController::MoveForward);
    InputComponent->BindAxis("MoveRight", this, &AFPSCharacterController::MoveRight);
    
    // We have 2 versions of the rotation bindings to handle different kinds of devices differently
    // "turn" handles devices that provide an absolute delta, such as a mouse.
    // "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
    InputComponent->BindAxis("Turn", this, &AFPSCharacterController::MakeAddControllerYawInput);
    InputComponent->BindAxis("TurnRate", this, &AFPSCharacterController::TurnAtRate);
    InputComponent->BindAxis("LookUp", this, &AFPSCharacterController::MakeAddControllerPitchInput);
    InputComponent->BindAxis("LookUpRate", this, &AFPSCharacterController::LookUpAtRate);
}

void AFPSCharacterController::MakeJump(){
    APawn* const Pawn = GetPawn();
    if(Pawn){
        ACharacter* character = Cast<ACharacter>(Pawn);
        if(character){
            character->Jump();
        }
    }
}

void AFPSCharacterController::MakeStopJump(){
    APawn* const Pawn = GetPawn();
    if(Pawn){
        ACharacter* character = Cast<ACharacter>(Pawn);
        if(character){
            character->StopJumping();
        }
    }
}

void AFPSCharacterController::MakeAddControllerYawInput(float Value){
    APawn* const Pawn = GetPawn();
    if(Pawn){
        Pawn->AddControllerYawInput(Value);
    }
}

void AFPSCharacterController::MakeAddControllerPitchInput(float Value){
    APawn* const Pawn = GetPawn();
    if(Pawn){
        Pawn->AddControllerPitchInput(Value);
    }
}

bool AFPSCharacterController::OnFire_Validate(){
    return true;
}

// [Server] Spawn a new projectile
// UFUNCTION(Reliable, Server, WithValidation)
void AFPSCharacterController::OnFire_Implementation(){
    // [Server] try and fire a projectile
    if(Role == ROLE_Authority){
        if(ProjectileClass != NULL) {
            const FRotator SpawnRotation = GetControlRotation();
            // MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
            const FVector SpawnLocation = GetPawn()->GetActorLocation() + SpawnRotation.RotateVector(GunOffset);
            FActorSpawnParameters params;
            params.Owner = this;
            params.Instigator = Instigator;
            UWorld* const World = GetWorld();
            if(World != NULL){
                // spawn the projectile at the muzzle
                AProjectile* NewProjectile = World->SpawnActor<AProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, params);
                if(!NewProjectile){
                    return;
                }
                NewProjectile->TeamNum = GetTeamNum();
                NewProjectile->SetReplicates(true);
            }
        }
    }
    /*
     // try and play the sound if specified
     if(FireSound != NULL){
     UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
     }
     
     // try and play a firing animation if specified
     if(FireAnimation != NULL){
     // Get the animation object for the arms mesh
     UANIMINSTANCE* AnimInstance = Mesh1P->GetAnimInstance();
     if(AnimInstance != NULL){
     AnimInstance->Montage_Play(FireAnimation, 1.f);
     }
     }*/
}

void AFPSCharacterController::TurnAtRate(float Rate){
    // calculate delta for this frame from the rate info
    AddYawInput(Rate*BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AFPSCharacterController::LookUpAtRate(float Rate){
    // calculate delta for this frame from the rate info
    AddPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AFPSCharacterController::MoveForward(float Value){
    if(Value != 0.0f){
        // add movement in that direction
        APawn* const Pawn = GetPawn();
        if(Pawn){
            Pawn->AddMovementInput(GetActorForwardVector(), Value);
        }
    }
}

void AFPSCharacterController::MoveRight(float Value){
    if(Value != 0.0f){
        // add movement in that direction
        APawn* const Pawn = GetPawn();
        if(Pawn){
            Pawn->AddMovementInput(GetActorRightVector(), Value);
        }
    }
}

//bool AFPSCharacterController::TransferPointsToGod_Validation() { return true; }
void AFPSCharacterController::TransferPointsToGod(){
    AGodsAndMonstersGameMode* GM = GetWorld()->GetAuthGameMode<AGodsAndMonstersGameMode>();
    if(GM){
        TArray<AGodPlayerController*> GodList = GM->GodList;
        for(AGodPlayerController* GodController : GodList){
            if(GodController->GetTeamNum() == GetTeamNum()){
                GodController->AddPoints(50);
            }
        }
    }
}

void AFPSCharacterController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    
    DOREPLIFETIME(AFPSCharacterController, PlayerNum);
}

uint32 AFPSCharacterController::GetTeamNum(){
    if(PlayerNum == 0 || PlayerNum == 1){
        return 0;
    }else{
        return 1;
    }
}