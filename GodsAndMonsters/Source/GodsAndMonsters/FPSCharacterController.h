// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Character.h"
#include "Projectile.h"
#include "FPSCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class GODSANDMONSTERS_API AFPSCharacterController : public APlayerController
{
	GENERATED_BODY()
    
public:
    AFPSCharacterController(const FObjectInitializer& ObjectInitializer);
    
    /** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
    float BaseTurnRate;
    
    /** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
    float BaseLookUpRate;
    
    /** Gun muzzle's offset from the characters location */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
    FVector GunOffset;
    
    /** Projectile class to spawn */
    UPROPERTY(EditDefaultsOnly, Category = ProjectileClass)
    TSubclassOf<class AProjectile> ProjectileClass;
    
    UPROPERTY(Replicated)
    uint32 PlayerNum;
    
    uint32 GetTeamNum();
    
//    UFUNCTION(Reliable, Server, WithValidation)
    void TransferPointsToGod();
//    bool TransferPointsToGod_Validation();
//    void TransferPointsToGod_Implementation();
    
    /** Sound to play each time we fire */
    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
    //class USoundBase* FireSound;
    
    /** AnimMontage to play each time we fire */
    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
    //class UAnimMontage* FireAnimation;
    
protected:
    // begin PlayerController interface
    virtual void SetupInputComponent() override;
    // End of PlayerController interface
    
    /** Fires a projectile */
    UFUNCTION(Reliable, Server, WithValidation)
    void OnFire();
    bool OnFire_Validate();
    void OnFire_Implementation();
    
    /** Called for forwards/backward input */
    void MoveForward(float Value);
    
    /** Called for side to side input */
    void MoveRight(float Value);
    
    /**
     * Called via input to turn at a given rate.
     * @param Rate  This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
     */
    void TurnAtRate(float Rate);
    
    /**
     * Called via input to turn look up/down at a given rate.
     * @param Rate  This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
     */
    void LookUpAtRate(float Rate);
    
    void MakeJump();
    void MakeStopJump();
    
    void MakeAddControllerYawInput(float Value);
    void MakeAddControllerPitchInput(float Value);
};
