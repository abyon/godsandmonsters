// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "FPSCharacterController.h"
#include "FirstPersonCharacter.h"
#include "GodsAndMonstersGameMode.h"


// Sets default values
AFirstPersonCharacter::AFirstPersonCharacter(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    // Set size for collision capsule
    GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
    
    TotalPoints = 0.0f;
    PlayerHealth = 70.0f;
    PlayerIsDead = false;
    
    // Create a mesh compnent that will be used when being viewed from a '1st person' view (when controlling this pawn)
    Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
    Mesh1P->AttachParent = GetCapsuleComponent();
    Mesh1P->RelativeLocation = FVector(0.f, 0.f, -150.f);
    Mesh1P->bCastDynamicShadow = false;
    Mesh1P->CastShadow = false;
    
    // Create a CameraComponent parented to head of mesh
    FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
    FirstPersonCameraComponent->AttachParent = Mesh1P;
    FirstPersonCameraComponent->AttachSocketName = TEXT("head");
    FirstPersonCameraComponent->RelativeRotation = FRotator(0.f, 90.f, -90.f);
    FirstPersonCameraComponent->bUsePawnControlRotation = true;
    
    // Replication
    bReplicates = true;
    bReplicateMovement = true;
    bNetLoadOnClient = true;
    //bNetUseOwnerRelevancy = true;
}

void AFirstPersonCharacter::BeginPlay()
{
    //Supers original begin play
    Super::BeginPlay();
    
    RespawnLocation = GetActorLocation();
    
}

// [Server to AllClient] Multiplayer Replication
void AFirstPersonCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps)const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    
    DOREPLIFETIME(AFirstPersonCharacter, PlayerHealth);
    DOREPLIFETIME(AFirstPersonCharacter, TotalPoints);
    DOREPLIFETIME(AFirstPersonCharacter, PlayerIsDead);
}


float AFirstPersonCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
    float ActualDamage =  Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
    
    if (ActualDamage > 0.0f) {
        PlayerHealth -= ActualDamage;
        if (PlayerHealth <= 0.0f) {
            bCanBeDamaged = false;
            
            AFPSCharacterController* MyPlayerController = Cast<AFPSCharacterController>(GetController());
            
            MyPlayerController->SetCinematicMode(true, true , true);
            
            //DeadCharacter();
            //IsDead();
            GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("YOU DIED!"));
            GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Respawning..."));
            GetWorldTimerManager().SetTimer(this, &AFirstPersonCharacter::DeadCharacter, 0.25f);
            
        }
    }
    
    return ActualDamage;
}

bool AFirstPersonCharacter::IsDead(){
    
    if (PlayerHealth <= 0.0f) {
        PlayerIsDead = true;
        return true;
    }
    
    return false;
}

bool AFirstPersonCharacter::DeadCharacter_Validate() { return true; }
void AFirstPersonCharacter::DeadCharacter_Implementation()
{
    if (Role == ROLE_Authority) {
        
        if (IsDead()) {
            
            if(Controller) {
                
                if(FPSCharacterClass != NULL) {
                    
                    const FRotator SpawnRotation = GetActorRotation();
                    const FVector shiftVector = FVector(0, 0, 70.0f);
                    const FVector SpawnLocation = RespawnLocation;
                    FActorSpawnParameters params;
                    params.Owner = this;
                    params.Instigator = Instigator;
                    UWorld* const World = GetWorld();
                   
                    if(World != NULL) {
                        
                        AFPSCharacterController* MyPlayerController = Cast<AFPSCharacterController>(GetController());
                        
                        if (MyPlayerController) {
                            
                            
                            Controller->UnPossess();
                            Destroy();
                            
                            float time = 3.0f;

                            //FPlatformProcess::Sleep(time);
                            
                            AFirstPersonCharacter *RespwanedCharacter = World->SpawnActor<AFirstPersonCharacter>(FPSCharacterClass, SpawnLocation, SpawnRotation, params);
                            MyPlayerController->Possess(RespwanedCharacter);
                        }
                    }
                }
            }
        }
        
    }
}


void AFirstPersonCharacter::CharacterDeath(){
    
    
    
}