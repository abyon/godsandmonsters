// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Projectile.h"
#include "FirstPersonCharacter.generated.h"

UCLASS(config=Game)
class GODSANDMONSTERS_API AFirstPersonCharacter : public ACharacter
{
	GENERATED_BODY()
    
    /** Pawn mesh: 1st person view */
    UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
    class USkeletalMeshComponent* Mesh1P;
    
    /** FPS Camera */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* FirstPersonCameraComponent;
    
public:
    // Constructor
	AFirstPersonCharacter(const FObjectInitializer& ObjectInitializer);
    
    UPROPERTY(Replicated, VisibleAnywhere, Category = Player)
    float TotalPoints;
    
    UFUNCTION(Reliable, NetMulticast, WithValidation)
    void DeadCharacter();
    bool DeadCharacter_Validate();
    void DeadCharacter_Implementation();
    
    virtual void BeginPlay() override;
    
    FVector RespawnLocation;
    
    float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
                             AController* EventInstigator, AActor* DamageCauser) override;

    void CharacterDeath();
    
    UPROPERTY(Replicated, EditAnywhere, Category = Player)
    float PlayerHealth;
    
    UPROPERTY(Replicated, BlueprintReadOnly, Category = Player)
    bool PlayerIsDead;
    
    UPROPERTY(EditDefaultsOnly, Category = Player)
    TSubclassOf<class ACharacter> FPSCharacterClass;
    
    bool IsDead();
    
    /** Returns Mesh1P subobject */
    FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
    
    /** Returns FollowCamera subobject **/
    FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const {
        return FirstPersonCameraComponent;
    }
};