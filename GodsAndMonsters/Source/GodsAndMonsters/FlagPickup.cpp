// Fill out your copyright notice in the Description page of Project Settings.


#include "GodsAndMonsters.h"
#include "GodsAndMonstersGameMode.h"
#include "FlagPickup.h"


AFlagPickup::AFlagPickup(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
    
    Temp = 0.0f;
    
    SetActorEnableCollision(true);
    
    BaseCollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    BaseCollisionComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
    BaseCollisionComponent->SetNotifyRigidBodyCollision(true);
    BaseCollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AFlagPickup::OnFlagCapture);
    
    // Replication
    bReplicates = true;
    bReplicateMovement = true;
}

void AFlagPickup::OnFlagCapture(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
    //OtherActor.TotalPoints += 10.0f;
    //get actor team # -
    //Give points to team # god
    
    OnPickedUp();
}

void AFlagPickup::OnPickedUp_Implementation()
{
    // Call the parent implementation of OnPickedUp first.
    Super::OnPickedUp_Implementation();
    AGodsAndMonstersGameMode* MyGameMode = Cast<AGodsAndMonstersGameMode>(UGameplayStatics::GetGameMode(this));
    if (MyGameMode) {
        //if we are currently playing
        MyGameMode->SetCurrentState(EGodsAndMonstersPlayState::EGameOver);
    }
   
    ProcessFlagPickup();
}

bool AFlagPickup::ProcessFlagPickup_Validate(){ return true; }
void AFlagPickup::ProcessFlagPickup_Implementation(){
    //Get Collided Character
    //Give him PointsAmount to Character's Total Points
    
    Destroy(); // Destroy the pickup
}