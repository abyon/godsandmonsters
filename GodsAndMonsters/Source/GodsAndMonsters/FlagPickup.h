// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Pickup.h"
#include "FlagPickup.generated.h"

/**
 * 
 */
UCLASS()
class GODSANDMONSTERS_API AFlagPickup : public APickup
{
	GENERATED_BODY()
	
public:
    AFlagPickup(const FObjectInitializer& ObjectInitializer);
    
    /** Set the amount of power the battery gives to the player. */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Flag)
    float Temp;
    
    /** Override the OnPickedUp function (use Implementation because this is a BlueprintNativeEvent). */
    void OnPickedUp_Implementation() override;
    
    //[Server]
    UFUNCTION(Reliable, Server, WithValidation)
    void ProcessFlagPickup();
    bool ProcessFlagPickup_Validate();
    void ProcessFlagPickup_Implementation();
    
    UFUNCTION()
    void OnFlagCapture(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit);

};

