// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "FirstPersonCharacter.h"
#include "GodCharacter.h"
#include "GodsAndMonstersGameMode.h"
#include "GMHUD.h"


AGMHUD::AGMHUD(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
    //Use the RobotoDistanceField font from the engine
    static ConstructorHelpers::FObjectFinder<UFont>HUDFontOb(TEXT("/Engine/EngineFonts/RobotoDistanceField"));
    HUDFont = HUDFontOb.Object;
}

void AGMHUD::DrawHUD()
{
    //Get the screen dimensions
    FVector2D ScreenDimensions = FVector2D(Canvas->SizeX, Canvas->SizeY);
    
    //Call to the parent versions of DrawHUD
    Super::DrawHUD();
    
    //Get the character and print its power level
    /*AFirstPersonCharacter* PlayerOne = Cast<AFirstPersonCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
    if (PlayerOne) {
        FString PowerLevelString = FString::Printf(TEXT("My God Points: %10.1f"), FMath::Abs(PlayerOne->TotalPoints));
        DrawText(PowerLevelString, FColor::White, 50, 50, HUDFont);
    }*/
    
    AFPSCharacterController* FPSController = Cast<AFPSCharacterController>(GetOwningPlayerController());
    //AGodsAndMonstersGameMode* MyGameMode = Cast<AGodsAndMonstersGameMode>(UGameplayStatics::GetGameMode(this));
    AGodsAndMonstersGameMode* MyGameMode = GetWorld()->GetAuthGameMode<AGodsAndMonstersGameMode>();

    if(FPSController){
        FString str = FString::Printf(TEXT("TeamNum: %d"), FPSController->GetTeamNum());
        DrawText(str, FColor::White, 50, 50, HUDFont);
        AFirstPersonCharacter* Monster = Cast<AFirstPersonCharacter>(FPSController->GetPawn());
        if(Monster){
            str = FString::Printf(TEXT("Health: %10.1f"), Monster->PlayerHealth);
            DrawText(str, FColor::White, 50, 70, HUDFont);
        }
        DrawText(TEXT("+"), FColor::White, Canvas->SizeX/2, Canvas->SizeY/2);
    }
    AGodPlayerController* GodController = Cast<AGodPlayerController>(GetOwningPlayerController());
    if(GodController){
        FString str = FString::Printf(TEXT("TeamNum: %d"), GodController->GetTeamNum());
        DrawText(str, FColor::White, 50, 50, HUDFont);
        str = FString::Printf(TEXT("Points: %d"), GodController->GetTotalPoints());
        DrawText(str, FColor::White, 50, 70, HUDFont);
        if(GodController->GetInventory() == 1){
            str = FString::Printf(TEXT("Current Spawn Tower: Turret Tower (Cost 50 pts)"));
            DrawText(str, FColor::White, 50, 90, HUDFont);
        }else if(GodController->GetInventory() == 2){
            str = FString::Printf(TEXT("Current Spawn Tower: Creep Tower (Cost 70 pts)"));
            DrawText(str, FColor::White, 50, 90, HUDFont);
        }else if(GodController->GetInventory() == 3){
            str = FString::Printf(TEXT("Current Spawn Tower: Barrier (Cost 20 pts)"));
            DrawText(str, FColor::White, 50, 90, HUDFont);
        }
    }
    
    if(MyGameMode){
        if(MyGameMode->GetCurrentState() == EGodsAndMonstersPlayState::EGameOver){
            DrawText(TEXT("GAME OVER"), FColor::White, 50, 120, HUDFont);
        }
    }
    
    // Enemy Data
//    AFirstPersonCharacter* PlayerTwo = Cast<AFirstPersonCharacter>(UGameplayStatics::GetPlayerPawn(this, 1));
//    if (PlayerTwo) {
//        FString PowerLevelString = FString::Printf(TEXT("Enemy God Points: %10.1f"), FMath::Abs(PlayerTwo->TotalPoints));
//        DrawText(PowerLevelString, FColor::White, 50, 100, HUDFont);
//    }

//    AGodsAndMonstersGameMode* MyGameMode = Cast<AGodsAndMonstersGameMode>(UGameplayStatics::GetGameMode(this));
//    //if the game is over
//    if (MyGameMode) {
//        if (MyGameMode->GetCurrentState() == EGodsAndMonstersPlayState::EGameOver)
//        {
//            // create a variable for storing the size of printing Game Over
//            /*FVector2D GameOverSize;
//            GetTextSize(TEXT("GAME OVER"), GameOverSize.X, GameOverSize.Y, HUDFont);
//            
//            if(GodController){
//                DrawText(TEXT("GAME OVER"), FColor::White, 50, 120, HUDFont);
//            }
//            if(FPSController){
//                AFirstPersonCharacter* Monster = Cast<AFirstPersonCharacter>(FPSController->GetPawn());
//                if(Monster){
//                    DrawText(TEXT("GAME OVER"), FColor::White, 50, 120, HUDFont);
//                }
//            }*/
//            DrawText(TEXT("GAME OVER"), FColor::White, 50, 120, HUDFont);
//
//        }
//    }
}