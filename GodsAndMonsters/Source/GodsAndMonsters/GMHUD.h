// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "GMHUD.generated.h"

/**
 * 
 */
UCLASS()
class GODSANDMONSTERS_API AGMHUD : public AHUD
{
	GENERATED_BODY()
public:
    
    AGMHUD(const FObjectInitializer& ObjectInitializer);
    
    /** Variable for storing the font */
    UPROPERTY()
    UFont* HUDFont;
    
    /** Primary draw call for the HUD */
    virtual void DrawHUD() OVERRIDE;
	
	
	
};
