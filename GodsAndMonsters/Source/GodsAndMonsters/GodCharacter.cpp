// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "GodCharacter.h"
#include "ItemSpawningPlatform.h"


// Sets default values
AGodCharacter::AGodCharacter(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    // Create dummy root component
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
    
    // Create a camera boom
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->AttachTo(RootComponent);
    CameraBoom->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 0.0f), FRotator(-60.f, 0.0f, 0.0f));
    //CameraBoom->bAbsoluteRotation = true;   // Don't want arm to rotate
    CameraBoom->TargetArmLength = 800.0f;
    CameraBoom->bDoCollisionTest = false;
    
    // Create a CameraComponent
    TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
    TopDownCameraComponent->AttachTo(CameraBoom, USpringArmComponent::SocketName);
    
    // Replication
    bReplicates = true;
    bReplicateMovement = true;
    bNetUseOwnerRelevancy = true;
}

// Called when the game starts or when spawned
void AGodCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGodCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}
