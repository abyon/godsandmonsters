// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "GodPlayerController.h"
#include "GodCharacter.h"
#include "ItemSpawningPlatform.h"

AGodPlayerController::AGodPlayerController(const FObjectInitializer& ObjectInitializer)
    :Super(ObjectInitializer)
{
    // Set defaults
    CamSpeed = 1000.0f;
    DefaultCamBoomLength = 800.0f;
    MaxCamBoomLength = 1000.0f;
    MinCamBoomLength = 300.0f;
    CamZoomSpeed = 50.0f;
    
    InventoryNum = 1;
    TotalPoints = 100;
    
    // Show mouse cursor
    bShowMouseCursor = true;
    bEnableClickEvents = true;
    bAlwaysRelevant = true;
}

void AGodPlayerController::BeginPlay(){
    Super::BeginPlay();
}

void AGodPlayerController::SetupInputComponent(){
    Super::SetupInputComponent();
    
    InputComponent->BindAxis("MoveForward", this, &AGodPlayerController::MoveUp);
    InputComponent->BindAxis("MoveRight", this, &AGodPlayerController::MoveRight);
    
    InputComponent->BindAction("MoveIn", IE_Pressed, this, &AGodPlayerController::MoveIn);
    InputComponent->BindAction("MoveOut", IE_Pressed, this, &AGodPlayerController::MoveOut);
    InputComponent->BindAction("GodPlaceObj", IE_Pressed, this, &AGodPlayerController::QueryHit);
    InputComponent->BindAction("InventorySwitch1", IE_Pressed, this, &AGodPlayerController::InventorySwitch1);
    InputComponent->BindAction("InventorySwitch2", IE_Pressed, this, &AGodPlayerController::InventorySwitch2);
    InputComponent->BindAction("InventorySwitch3", IE_Pressed, this, &AGodPlayerController::InventorySwitch3);
}

void AGodPlayerController::QueryHit(){
    static FName SpawnItemTag = FName(TEXT("SpawnItem"));
    AGodCharacter* GodCharacter = Cast<AGodCharacter>(GetPawn());
    if(GodCharacter){
        FHitResult Hit;
        GetHitResultUnderCursor(ECC_Visibility, false, Hit);
        
        if(Hit.bBlockingHit){
            AItemSpawningPlatform* platform = Cast<AItemSpawningPlatform>(Hit.GetActor());
            if(platform){
                SpawnItem(platform);
            }
        }
    }
}

bool AGodPlayerController::SpawnItem_Validate(AItemSpawningPlatform* platform){
    return true;
}

void AGodPlayerController::SpawnItem_Implementation(AItemSpawningPlatform* platform){
    static FName SpawnItemTag = FName(TEXT("SpawnItem"));
    if(Role == ROLE_Authority){
        if(platform){
            // Spawn Something
            if(TempItem != NULL){
                const FRotator SpawnRotation = GetControlRotation();
                FActorSpawnParameters params;
                params.Owner = this;
                params.Instigator = Instigator;
                APlaceable* NewItem = NULL;
                if(!platform->Occupied){
                    if(InventoryNum == 1){
                        if(TotalPoints >= 50){
                            NewItem = GetWorld()->SpawnActor<APlaceable>(TempItem, platform->SpawnPoint+platform->GetActorLocation(), SpawnRotation, params);
                            TotalPoints -= 50;
                        }
                    }else if(InventoryNum == 2){
                        if(TotalPoints >= 70){
                            NewItem = GetWorld()->SpawnActor<APlaceable>(TempItem2, platform->SpawnPoint+platform->GetActorLocation(), SpawnRotation, params);
                            TotalPoints -= 70;
                        }
                    }else if(InventoryNum == 3){
                        if(TotalPoints >= 20){
                            NewItem = GetWorld()->SpawnActor<APlaceable>(TempItem3, platform->SpawnPoint+platform->GetActorLocation(), SpawnRotation, params);
                            TotalPoints -= 20;
                        }
                    }
                    
                    if(NewItem){
                        NewItem->SetReplicates(true);
                        NewItem->TeamNum = GetTeamNum();
                        NewItem->platform = platform;
                        platform->Occupied = true;
                    }
                }
            }
        }
    }
}

void AGodPlayerController::MoveUp(float Value){
    CameraInput.X = FMath::Clamp<float>(Value, -1.0f, 1.0f);
}

void AGodPlayerController::MoveRight(float Value){
    CameraInput.Y = FMath::Clamp<float>(Value, -1.0f, 1.0f);
}

void AGodPlayerController::MoveIn(){
    AGodCharacter* GodCharacter = Cast<AGodCharacter>(GetPawn());
    if(GodCharacter){
        if(!(GodCharacter->GetCamBoomComponent()->TargetArmLength - CamZoomSpeed < MinCamBoomLength)){
            GodCharacter->GetCamBoomComponent()->TargetArmLength -= CamZoomSpeed;
        }
    }
}

void AGodPlayerController::MoveOut(){
    AGodCharacter* GodCharacter = Cast<AGodCharacter>(GetPawn());
    if(GodCharacter){
        if(!(GodCharacter->GetCamBoomComponent()->TargetArmLength + CamZoomSpeed > MaxCamBoomLength)){
            GodCharacter->GetCamBoomComponent()->TargetArmLength += CamZoomSpeed;
        }
    }
}

bool AGodPlayerController::InventorySwitch1_Validate() { return true; }
void AGodPlayerController::InventorySwitch1_Implementation(){
    InventoryNum = 1;
}

bool AGodPlayerController::InventorySwitch2_Validate() { return true; }
void AGodPlayerController::InventorySwitch2_Implementation(){
    InventoryNum = 2;
}

bool AGodPlayerController::InventorySwitch3_Validate() { return true; }
void AGodPlayerController::InventorySwitch3_Implementation(){
    InventoryNum = 3;
}

void AGodPlayerController::PlayerTick(float DeltaTime){
    Super::PlayerTick(DeltaTime);
    
    if(!CameraInput.IsZero()){
        APawn* const Pawn = GetPawn();
        if(Pawn){
            // Scale movement input by CamSpeed
            CameraInput = CameraInput.GetSafeNormal() * CamSpeed;
            
            FVector NewLocation = Pawn->GetActorLocation();
            NewLocation += Pawn->GetActorForwardVector() * CameraInput.X * DeltaTime;
            NewLocation += Pawn->GetActorRightVector() * CameraInput.Y * DeltaTime;
            Pawn->SetActorLocation(NewLocation);
        }
    }
}

void AGodPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    
    DOREPLIFETIME(AGodPlayerController, PlayerNum);
    DOREPLIFETIME(AGodPlayerController, InventoryNum);
    DOREPLIFETIME(AGodPlayerController, TotalPoints);
}

void AGodPlayerController::AddPoints(uint32 numPoints){
    TotalPoints += numPoints;
}

uint32 AGodPlayerController::GetTeamNum(){
    if(PlayerNum == 0 || PlayerNum == 1){
        return 0;
    }else{
        return 1;
    }
}

uint32 AGodPlayerController::GetTotalPoints(){
    return TotalPoints;
}

uint32 AGodPlayerController::GetInventory(){
    return InventoryNum;
}