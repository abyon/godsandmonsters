// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "ItemSpawningPlatform.h"
#include "GodPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class GODSANDMONSTERS_API AGodPlayerController : public APlayerController
{
	GENERATED_BODY()
    
    AGodPlayerController(const FObjectInitializer& ObjectInitializer);
    
    /** Temp item to place */
    UPROPERTY(EditDefaultsOnly, Category=ItemPlaced)
    TSubclassOf<class APlaceable> TempItem;
    
    /** Temp item to place */
    UPROPERTY(EditDefaultsOnly, Category=ItemPlaced)
    TSubclassOf<class APlaceable> TempItem2;
    
    UPROPERTY(EditDefaultsOnly, Category=ItemPlaced)
    TSubclassOf<class APlaceable> TempItem3;
    
public:
    UPROPERTY(Replicated)
    uint32 PlayerNum;
    
    uint32 GetTeamNum();
    
    uint32 GetTotalPoints();
    
    uint32 GetInventory();
    
    void AddPoints(uint32 numPoints);
	
protected:
    // begin PlayerController interface
    virtual void PlayerTick(float DeltaTime) override;
    virtual void SetupInputComponent() override;
    // end PlayerController interface
    
    /** Navigate player using WASD controls */
    void MoveUp(float Value);
    void MoveRight(float Value);
    
    /** Allow cam to zoom in or out */
    void MoveIn();
    void MoveOut();
    
    /** Functions to change inventory */
    UFUNCTION(Reliable, Server, WithValidation)
    void InventorySwitch1();
    bool InventorySwitch1_Validate();
    void InventorySwitch1_Implementation();
    
    UFUNCTION(Reliable, Server, WithValidation)
    void InventorySwitch2();
    bool InventorySwitch2_Validate();
    void InventorySwitch2_Implementation();
    
    UFUNCTION(Reliable, Server, WithValidation)
    void InventorySwitch3();
    bool InventorySwitch3_Validate();
    void InventorySwitch3_Implementation();
    
    /** Local Query to see what we hit and send that info to server to verify that we can spawn something at a platform or not */
    void QueryHit();
    
    /** Called for placing objects in the world */
    UFUNCTION(Reliable, Server, WithValidation)
    void SpawnItem(AItemSpawningPlatform* platform);
    bool SpawnItem_Validate(AItemSpawningPlatform* platform);
    void SpawnItem_Implementation(AItemSpawningPlatform* platform);
    
    // Direction for camera to move depending on Inputs
    FVector2D CameraInput;
    
    // Speed for camera to move
    UPROPERTY(EditDefaultsOnly, Category=Camera)
    float CamSpeed;
    
    // Default height camera is above the floor
    UPROPERTY(EditDefaultsOnly, Category = Camera)
    float DefaultCamBoomLength;
    
    // Maximum height camera is above the floor
    UPROPERTY(EditDefaultsOnly, Category = Camera)
    float MaxCamBoomLength;
    
    // Minimum height camera is above the floor
    UPROPERTY(EditDefaultsOnly, Category = Camera)
    float MinCamBoomLength;
    
    // Rate to change camera boom height
    UPROPERTY(EditDefaultsOnly, Category = Camera)
    float CamZoomSpeed;
    
    UPROPERTY(Replicated)
    uint32 InventoryNum;
    
    UPROPERTY(Replicated)
    uint32 TotalPoints;
    
    virtual void BeginPlay() override;
    
};