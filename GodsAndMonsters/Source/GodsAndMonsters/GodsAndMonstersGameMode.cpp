// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "GodsAndMonsters.h"
#include "GodsAndMonstersGameMode.h"
#include "GodsAndMonstersCharacter.h"
#include "GMHUD.h"
#include "FirstPersonCharacter.h"
#include "GodPlayerController.h"

AGodsAndMonstersGameMode::AGodsAndMonstersGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
    /*static ConstructorHelpers::FClassFinder<APlayerController> DefaultPlayerController(TEXT("/Game/ThirdPerson/Blueprints/FPSController"));
    if(DefaultPlayerController.Class != NULL){
        PlayerControllerClass = DefaultPlayerController.Class;
    }
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/FirstPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}*/
    
    static ConstructorHelpers::FClassFinder<AFPSCharacterController> FoundFPSController(TEXT("/Game/ThirdPerson/Blueprints/FPSController"));
    if(FoundFPSController.Class != NULL){
        FPSController = FoundFPSController.Class;
    }
    static ConstructorHelpers::FClassFinder<AGodPlayerController> FoundGodController(TEXT("/Game/ThirdPerson/Blueprints/GodPlayerController"));
    if(FoundGodController.Class != NULL){
        GodController = FoundGodController.Class;
    }
    static ConstructorHelpers::FClassFinder<AFirstPersonCharacter> FoundFPSCharacter(TEXT("/Game/ThirdPerson/Blueprints/FirstPersonCharacter"));
    if(FoundFPSCharacter.Class != NULL){
        FPSCharacter = FoundFPSCharacter.Class;
    }
    static ConstructorHelpers::FClassFinder<AGodCharacter> FoundGodCharacter(TEXT("/Game/ThirdPerson/Blueprints/GodCharacter"));
    if(FoundGodCharacter.Class != NULL){
        GodCharacter = FoundGodCharacter.Class;
    }
    
    // will fully implement later
    /*static ConstructorHelpers::FClassFinder<AHUDGod> FoundHUDGod(TEXT("/Game/ThirdPerson/Blueprints/HUDGod"));
    if(FoundHUDGod.Class != NULL){
        HUDGod = FoundHUDGod.Class;
    }
    static ConstructorHelpers::FClassFinder<AHUDMonster> FoundHUDMonster(TEXT("/Game/ThirdPerson/Blueprints/HUDMonster"));
    if(FoundHUDMonster.Class != NULL){
        HUDMonster = FoundHUDMonster.Class;
    }*/
    
    NumPlayers = 0;
    
    SetCurrentState(EGodsAndMonstersPlayState::EPlaying);
    
    HUDClass = AGMHUD::StaticClass();
}

void AGodsAndMonstersGameMode::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}

APlayerController* AGodsAndMonstersGameMode::SpawnPlayerController(const FVector &SpawnLocation, const FRotator &SpawnRotation){
    FActorSpawnParameters SpawnInfo;
    SpawnInfo.Instigator = Instigator;
    if(NumPlayers%2 == 0){
        AGodPlayerController* SpawnedGodController = GetWorld()->SpawnActor<AGodPlayerController>(GodController, SpawnLocation, SpawnRotation, SpawnInfo);
        SpawnedGodController->PlayerNum = NumPlayers;
        GodList.Add(SpawnedGodController);
        NumPlayers++;
        return SpawnedGodController;
    }else{
        AFPSCharacterController* SpawnedFPSController = GetWorld()->SpawnActor<AFPSCharacterController>(FPSController, SpawnLocation, SpawnRotation, SpawnInfo);
        SpawnedFPSController->PlayerNum = NumPlayers;
        MonsterList.Add(SpawnedFPSController);
        NumPlayers++;
        return SpawnedFPSController;
    }
}

UClass* AGodsAndMonstersGameMode::GetDefaultPawnClassForController(AController *InController){
    AFPSCharacterController* FPSCharCont = Cast<AFPSCharacterController>(InController);
    AGodPlayerController* GodCharCont = Cast<AGodPlayerController>(InController);
    if(FPSCharCont){
        return FPSCharacter;
    }
    if(GodCharCont){
        return GodCharacter;
    }
    return Super::GetDefaultPawnClassForController(InController);
}

AActor* AGodsAndMonstersGameMode::ChoosePlayerStart(AController *Player){
    // Choose a player start
    APlayerStart* FoundPlayerStart = NULL;
    APawn* PawnToFit = Cast<APawn>(GetDefaultPawnClassForController(Player));
    TArray<APlayerStart*> UnOccupiedStartPoints;
    TArray<APlayerStart*> OccupiedStartPoints;
//    for (int32 PlayerStartIndex = 0; PlayerStartIndex < PlayerStarts.Num(); ++PlayerStartIndex)
//    {
//        APlayerStart* PlayerStart = PlayerStarts[PlayerStartIndex];
//        
//        if (Cast<APlayerStartPIE>( PlayerStart ) != NULL )
//        {
//            // Always prefer the first "Play from Here" PlayerStart, if we find one while in PIE mode
//            FoundPlayerStart = PlayerStart;
//            break;
//        }
//        else if (PlayerStart != NULL)
//        {
//            FVector ActorLocation = PlayerStart->GetActorLocation();
//            const FRotator ActorRotation = PlayerStart->GetActorRotation();
//            if (!GetWorld()->EncroachingBlockingGeometry(PawnToFit, ActorLocation, ActorRotation))
//            {
//                UnOccupiedStartPoints.Add(PlayerStart);
//            }
//            else if (GetWorld()->FindTeleportSpot(PawnToFit, ActorLocation, ActorRotation))
//            {
//                OccupiedStartPoints.Add(PlayerStart);
//            }
//        }
//    }
    if(NumPlayers == 1 || NumPlayers == 2){
        APlayerStart* PlayerStart = PlayerStarts[0];
        if(Cast<APlayerStart>(PlayerStart) != NULL){
            FoundPlayerStart = PlayerStart;
        }
    }else{
        APlayerStart* PlayerStart = PlayerStarts[1];
        if(Cast<APlayerStart>(PlayerStart) != NULL){
            FoundPlayerStart = PlayerStart;
        }
    }
    if (FoundPlayerStart == NULL)
    {
        if (UnOccupiedStartPoints.Num() > 0)
        {
            FoundPlayerStart = UnOccupiedStartPoints[FMath::RandRange(0, UnOccupiedStartPoints.Num() - 1)];
        }
        else if (OccupiedStartPoints.Num() > 0)
        {
            FoundPlayerStart = OccupiedStartPoints[FMath::RandRange(0, OccupiedStartPoints.Num() - 1)];
        }
    }
    return FoundPlayerStart;
}

void AGodsAndMonstersGameMode::BeginPlay()
{
    Super::BeginPlay();
    
    SetCurrentState(EGodsAndMonstersPlayState::EPlaying);
    
}

void AGodsAndMonstersGameMode::SetCurrentState(EGodsAndMonstersPlayState NewState)
{
    CurrentState = NewState;
    
    HandleNewState(NewState);
}



void AGodsAndMonstersGameMode::HandleNewState(EGodsAndMonstersPlayState NewState)
{
    switch (NewState)
    {
            // When we're playing, the spawn volumes can spawn
        case EGodsAndMonstersPlayState::EPlaying:
            break;
            
            // if the game is over, the spawn volumes should deactivate
        case EGodsAndMonstersPlayState::EGameOver:
        {
            UE_LOG(LogTemp, Warning, TEXT("GAME OVER"));
        }
            break;
            
        case EGodsAndMonstersPlayState::EUnknown:
        default:
            //do nothing
            break;
    }
}