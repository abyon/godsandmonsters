// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "FPSCharacterController.h"
#include "GodPlayerController.h"
#include "FirstPersonCharacter.h"
#include "GodCharacter.h"
#include "GMHUD.h"
#include "GodsAndMonstersGameMode.generated.h"

// Enum to store the current state of gameplay.
enum class EGodsAndMonstersPlayState : short
{
    EPlaying,
    EGameOver,
    EUnknown
};

UCLASS(minimalapi)
class AGodsAndMonstersGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AGodsAndMonstersGameMode(const FObjectInitializer& ObjectInitializer);
    
    virtual void Tick(float DeltaSeconds) override;
    
    virtual void BeginPlay() override;
    
    EGodsAndMonstersPlayState GetCurrentState() const;
    
    void SetCurrentState(EGodsAndMonstersPlayState NewState);
    
    int32 NumPlayers;
    
    TArray<AFPSCharacterController*> MonsterList;
    
    TArray<AGodPlayerController*> GodList;
    
private:
    //TArray<ASpawnVolume*> SpawnVolumeActors;
    
    EGodsAndMonstersPlayState CurrentState;
    
    void HandleNewState(EGodsAndMonstersPlayState NewState);
    
    TSubclassOf<class APlayerController> FPSController;
    
    TSubclassOf<class APawn> FPSCharacter;
    
    TSubclassOf<class APlayerController> GodController;
    
    TSubclassOf<class APawn> GodCharacter;
    
    // need to flesh out more
    // TSubclassOf<class AHUD> HUDGod;
    // TSubclassOf<class AHUD> HUDMonster;

    
protected:
    APlayerController* SpawnPlayerController(FVector const& SpawnLocation, FRotator const& SpawnRotation) override;
    
    UClass* GetDefaultPawnClassForController(AController* InController) override;
    
    AActor* ChoosePlayerStart(AController* Player) override;
};

FORCEINLINE EGodsAndMonstersPlayState AGodsAndMonstersGameMode::GetCurrentState() const
{
    return CurrentState;
}
