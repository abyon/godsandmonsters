// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "ItemSpawningPlatform.h"


// Sets default values
AItemSpawningPlatform::AItemSpawningPlatform(const FObjectInitializer& ObjectInitializer)
    :Super(ObjectInitializer)
{
    CollisionComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
    CollisionComp->InitBoxExtent(FVector(50.f, 50.f, 50.f));
    //CollisionComp->BodyInstance.SetCollisionProfileName("Platform");
    //CollisionComp->OnComponentHit.AddDynamic(this, &AItemSpawningPlatform::SomeFunction);
    
    // Set as root component
    RootComponent = CollisionComp;
    
    SpawnPoint = FVector(0.f, 0.f, 60.f);
    
    Occupied = false;
    
    // Replication
    bReplicates = true;
    bAlwaysRelevant = true;
    bNetLoadOnClient = true;
}

// Called when the game starts or when spawned
void AItemSpawningPlatform::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AItemSpawningPlatform::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

bool AItemSpawningPlatform::GetOccupied(){
    return Occupied;
}

void AItemSpawningPlatform::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    
    //Replicate to everyone
    DOREPLIFETIME(AItemSpawningPlatform, Occupied);
}