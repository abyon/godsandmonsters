// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "UnrealNetwork.h"
#include "ItemSpawningPlatform.generated.h"

UCLASS(config=Game)
class GODSANDMONSTERS_API AItemSpawningPlatform : public AActor
{
	GENERATED_BODY()
    
    /** Rectangle collision component */
    UPROPERTY(VisibleDefaultsOnly, Category=ItemSpawningPlatform)
    class UBoxComponent* CollisionComp;
	
public:	
	// Sets default values for this actor's properties
	AItemSpawningPlatform(const FObjectInitializer& ObjectInitializer);
    
    /** FVector to place point of spawn in relation to local position */
    UPROPERTY(EditDefaultsOnly, Category=SpawnPoint)
    FVector SpawnPoint;
    
    /** Bool to know if there is already something spawned on here */
    UPROPERTY(Replicated)
    bool Occupied;
    
    UFUNCTION(BlueprintCallable, Category=Occupied)
    bool GetOccupied();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
    
};