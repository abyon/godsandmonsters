// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "Pickup.h"


APickup::APickup(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    
    // The pickup is valid when it is created.
    bIsActive = true;
    
    // Create the root SphereComponent to handle the pickup's collision
    BaseCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("BaseSphereComponent"));
    BaseCollisionComponent->InitSphereRadius(10.f);
    BaseCollisionComponent->BodyInstance.SetCollisionProfileName("Pickup");
    
    // Set the SphereComponent as the root component.
    RootComponent = BaseCollisionComponent;
    
    // Create the StaticMeshComponent.
    PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PickupMesh"));
    
    // Turn physics on for the static mesh.
    PickupMesh->SetSimulatePhysics(true);
    
    // Attach the StaticMeshComponent to the RootComponent.
    PickupMesh->AttachTo(RootComponent);
    
    SetActorEnableCollision(true);
    
}

void APickup::OnPickedUp_Implementation()
{
    // There is no default behavior for a Pickup when it is picked up.
}


// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickup::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

