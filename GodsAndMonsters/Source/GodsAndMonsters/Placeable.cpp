// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "Placeable.h"


// Sets default values
APlaceable::APlaceable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    bReplicates = true;
    bReplicateMovement = true;
    bNetLoadOnClient = true;
    bAlwaysRelevant = true;
}

// Called when the game starts or when spawned
void APlaceable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlaceable::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

