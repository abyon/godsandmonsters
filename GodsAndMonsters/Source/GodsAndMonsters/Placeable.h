// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "ItemSpawningPlatform.h"
#include "Placeable.generated.h"

UCLASS()
class GODSANDMONSTERS_API APlaceable : public APawn
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlaceable();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

    uint32 TeamNum;
    
    AItemSpawningPlatform* platform;
};
