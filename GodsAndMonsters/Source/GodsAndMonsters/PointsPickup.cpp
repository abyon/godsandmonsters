// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "FirstPersonCharacter.h"
#include "PointsPickup.h"
#include "FPSCharacterController.h"


APointsPickup::APointsPickup(const FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
    PrimaryActorTick.bCanEverTick = true;

    // The base power level of the battery.
    PointsAmount = 10.f;
   
    SetActorEnableCollision(true);
    
    // The pickup is valid when it is created.
    bIsActive = true;
    
    BaseCollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    BaseCollisionComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
    BaseCollisionComponent->SetNotifyRigidBodyCollision(true);
    BaseCollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &APointsPickup::OnMyActorHit);
    
    // Replication
    bReplicates = true;
    bReplicateMovement = true;
    bNetUseOwnerRelevancy = true;
}

void APointsPickup::OnMyActorHit(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
    
    AFirstPersonCharacter* MyCharacter = Cast<AFirstPersonCharacter>(OtherActor);
    if (MyCharacter) {
        AFPSCharacterController* CharacterController = Cast<AFPSCharacterController>(MyCharacter->GetController());
        if(CharacterController){
            CharacterController->TransferPointsToGod();
        }
        MyCharacter->TotalPoints += PointsAmount;
        OnPickedUp();
    }
}

void APointsPickup::OnPickedUp_Implementation()
{
    // Call the parent implementation of OnPickedUp first.
    Super::OnPickedUp_Implementation();
    
    Destroy();
    ProcessPointsPickup();
    
}

bool APointsPickup::ProcessPointsPickup_Validate(){ return true; }
void APointsPickup::ProcessPointsPickup_Implementation(){
    //Get Collided Character
    //Give him PointsAmount to Character's Total Points
    
    Destroy(); // Destroy the pickup
}