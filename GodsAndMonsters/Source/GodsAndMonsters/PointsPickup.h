// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Pickup.h"
#include "PointsPickup.generated.h"

/**
 * 
 */
UCLASS()
class GODSANDMONSTERS_API APointsPickup : public APickup
{
	GENERATED_BODY()
public:
    APointsPickup(const FObjectInitializer& ObjectInitializer);
    
    /** Set the amount of power the battery gives to the player. */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Power)
    float PointsAmount;
	
    /** Override the OnPickedUp function (use Implementation because this is a BlueprintNativeEvent). */
    void OnPickedUp_Implementation() override;
    
    //[Server]
    UFUNCTION(Reliable, Server, WithValidation)
    void ProcessPointsPickup();
    bool ProcessPointsPickup_Validate();
    void ProcessPointsPickup_Implementation();
    
    UFUNCTION()
    void OnMyActorHit(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit);
    
};
