// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "Projectile.h"
#include "FirstPersonCharacter.h"
#include "CreepCharacter.h"
#include "TowerPawn.h"
#include "Barrier.h"
#include "FPSCharacterController.h"
#include "GameFramework/ProjectileMovementComponent.h"


// Sets default values
AProjectile::AProjectile(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    // Use a sphere as a simple representation
    CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
    CollisionComp->InitSphereRadius(5.0f);
    CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
    CollisionComp->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);    // set up notification for when this component hits something blocking
    
    // Players can't walk on it
    CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
    CollisionComp->CanCharacterStepUpOn = ECB_No;
    
    // Set as root component
    RootComponent = CollisionComp;
    
    // Use a ProjectileMovementComponent to govern this projectile's movement
    ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
    ProjectileMovement->UpdatedComponent = CollisionComp;
    ProjectileMovement->InitialSpeed = 3000.f;
    ProjectileMovement->MaxSpeed = 3000.f;
    ProjectileMovement->bRotationFollowsVelocity = true;
    ProjectileMovement->bShouldBounce = true;
    ProjectileMovement->ProjectileGravityScale = 0;
    
    // Die after 3 seconds by default
    InitialLifeSpan = 3.0f;
    
    SetActorEnableCollision(true);
    
    // Replication
    bReplicates = true;
    bReplicateMovement = true;
    bNetLoadOnClient = true;
    bAlwaysRelevant = true;
}

void AProjectile::OnHit(AActor *OtherActor, UPrimitiveComponent *OtherComp, FVector NormalImpulse, const FHitResult &Hit){
    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("I hit something"));
    
    // Only add impulse and destroy the projectile if we hit a physics
    if((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) ){
        //&& OtherComp->IsSimulatingPhysics()
        //OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
        
        ACreepCharacter* Creep = Cast<ACreepCharacter>(OtherActor);
        ATowerPawn *projectileTower = Cast<ATowerPawn>(OtherActor);
        ABarrier *Barrier = Cast<ABarrier>(OtherActor);
        AFirstPersonCharacter* FPSChar = Cast<AFirstPersonCharacter>(OtherActor);
        
        if (Creep) {
            UE_LOG(LogTemp, Warning, TEXT("On HIT"));
            Creep->TakeDamage(20.0f, FDamageEvent(), GetInstigatorController(), this);
        }else if (projectileTower){
            if(projectileTower->TeamNum != TeamNum){
                projectileTower->TakeDamage(20.0f, FDamageEvent(), GetInstigatorController(), this);
            }
        }else if (Barrier){
            Barrier->TakeDamage(10.0f, FDamageEvent(), GetInstigatorController(), this);
        }else if(FPSChar){
            AFPSCharacterController* FPSCharCont = Cast<AFPSCharacterController>(FPSChar->GetController());
            if(FPSCharCont){
                if(FPSCharCont->GetTeamNum() != TeamNum){
                    FPSChar->TakeDamage(10.0f, FDamageEvent(), GetInstigatorController(), this);
                }
            }
        }
        
         //UE_LOG(LogTemp, Warning, TEXT("AFTER LOOP"));
        // Add Hit Effect aswell...
        Destroy();
    }
}