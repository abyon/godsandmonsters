// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "SpawnManager.h"
#include "PointsPickup.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"

// Sets default values
ASpawnManager::ASpawnManager(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
    MaxSpawnTime = 5.0f;
    MinSpawnTime = 2.0f;
}

//Called when the game starts or when spawned
void ASpawnManager::BeginPlay()
{
	Super::BeginPlay();
    
    // [Server]
    if (Role == ROLE_Authority) {
        
        float TimeToSpawn = FMath::FRandRange(MinSpawnTime, MaxSpawnTime);
        GetWorldTimerManager().SetTimer(this, &ASpawnManager::SpawnPickup, TimeToSpawn, true);
    }
	
}

// [Server] Spawn NextDwarf
bool ASpawnManager::SpawnPickup_Validate() { return true; }
void ASpawnManager::SpawnPickup_Implementation() {
    FActorSpawnParameters SpawnParams;
    SpawnParams.Owner = this;
    SpawnParams.Instigator = Instigator;
    
    
    if (PickupToSpawn != NULL)
    {
        // Check for a valid World:
        UWorld* const World = GetWorld();
        if (World)
        {
            // Set the spawn parameters.
            FActorSpawnParameters SpawnParams;
            SpawnParams.Owner = this;
            SpawnParams.Instigator = Instigator;
            
            //int ArraySize = sizeof(TargetPoints);
            int IndexToSpawn = FMath::FRandRange(0, 3);
            FVector SpawnLocation = TargetPoints[IndexToSpawn]->GetActorLocation();
            
            // Spawn the pickup.
            APointsPickup* const SpawnedPoints = World->SpawnActor<APointsPickup>(PickupToSpawn, SpawnLocation, FRotator::ZeroRotator, SpawnParams);
            
//            if (SpawnedPoints) {
//                SpawnedPoints->SpawnDefaultController();
//            }
            
        }
    }
    
    // [Server] Next SPawn
    float NextRandomSpawn = FMath::FRandRange(MinSpawnTime, MaxSpawnTime);
    GetWorldTimerManager().SetTimer(this, &ASpawnManager::SpawnPickup, NextRandomSpawn, false);
}