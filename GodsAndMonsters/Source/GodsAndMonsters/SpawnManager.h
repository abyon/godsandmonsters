// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "SpawnManager.generated.h"

UCLASS()
class GODSANDMONSTERS_API ASpawnManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
    ASpawnManager(const class FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawn)
    TArray<class ATargetPoint*> TargetPoints;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawn)
    TSubclassOf<class AActor> PickupToSpawn;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawn)
    float MinSpawnTime;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spawn)
    float MaxSpawnTime;
    
    void EnableSpawning();
    void DisableSpawning();
    
    /** Handles the spawning of a new pickup. */
        // [Server] Spawn NextPickup
    UFUNCTION(Reliable, Server, WithValidation)
    void SpawnPickup();
    bool SpawnPickup_Validate();
    void SpawnPickup_Implementation();
    
private:
    
    /** Whether or not spawning is enabled. */
    bool bSpawningEnabled;
    
    /** The timer for when to spawn the pickup. */
    float SpawnTime;
	
};
