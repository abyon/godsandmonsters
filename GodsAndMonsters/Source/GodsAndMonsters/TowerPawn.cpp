// Fill out your copyright notice in the Description page of Project Settings.

#include "GodsAndMonsters.h"
#include "AITowerController.h"
#include "TowerPawn.h"
#include "GodsAndMonstersGameMode.h"
#include "FPSCharacterController.h"


// Sets default values
ATowerPawn::ATowerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    AIControllerClass = AAITowerController::StaticClass();
    HP = 80.0f;
    TowerDamage = 5.0f;
    
    bReplicates = true;
    bReplicateMovement = true;
    
    CurrentFacing = FVector::ZeroVector;
    
    GunOffset = FVector(10.f, 0.f, 200.f);
    
    // The pickup is valid when it is created.
    bIsActive = true;
    
    SetActorEnableCollision(true);
    
    BaseDummyComponent = CreateDefaultSubobject<USceneComponent>(TEXT("BaseDummyComponent"));
    RootComponent = BaseDummyComponent;
    
    // Create the root SphereComponent to handle the pickup's collision
    BaseCollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BaseBoxComponent"));
    BaseCollisionComponent->InitBoxExtent(FVector(10.f, 10.f, 20.f));
    BaseCollisionComponent->SetRelativeLocation(FVector(0.f, 0.f, 200.f));
    BaseCollisionComponent->BodyInstance.SetCollisionProfileName("Tower");
    BaseCollisionComponent->AttachTo(RootComponent);
    
    // Set the SphereComponent as the root component.
    //RootComponent = BaseCollisionComponent;
}

// Called when the game starts or when spawned
void ATowerPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATowerPawn::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::red, TEXT("THING"));

}

// Called to bind functionality to input
void ATowerPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

bool ATowerPawn::StartAttack_Validate() { return true; }
void ATowerPawn::StartAttack_Implementation()
{
    
    //if (Role == ROLE_Authority)
    //{
        //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Attack Timer On"));
        GetWorldTimerManager().SetTimer(this, &ATowerPawn::FireAtMonster, FiringRate, true);
    //}
}

bool ATowerPawn::StopAttack_Validate() { return true; }
void ATowerPawn::StopAttack_Implementation()
{
    if (Role == ROLE_Authority)
    {
        //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Attack Timer Off"));
        GetWorldTimerManager().ClearTimer(this, &ATowerPawn::FireAtMonster);
    }
}

float ATowerPawn::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent,
                                  AController* EventInstigator, AActor* DamageCauser)
{
    float ActualDamage = Super::TakeDamage(Damage, DamageEvent,
                                           EventInstigator, DamageCauser);
    if (ActualDamage > 0.0f)
    {
        HP -= ActualDamage;
        if (HP <= 0.0f)
        {
            HP = 0.0f;
            // We're dead
            bCanBeDamaged = false; // Don't allow further damage
            
            // TODO: Process death
            DeadTower();
            
        }
    }
    return ActualDamage;
}

bool ATowerPawn::DeadTower_Validate() { return true; }
void ATowerPawn::DeadTower_Implementation()
{

    if (Role == ROLE_Authority)
    {
        if(Controller) {
            if(PointsPickupClass != NULL) {
                const FRotator SpawnRotation = GetActorRotation();
                const FVector shiftVector = FVector(0, 0, 70.0f);
                const FVector SpawnLocation = GetActorLocation() + shiftVector;
                FActorSpawnParameters params;
                params.Owner = this;
                params.Instigator = Instigator;
                UWorld* const World = GetWorld();
                if(World != NULL){
                    // spawn the pickup at dead creep
                    APointsPickup* NewPoints = World->SpawnActor<APointsPickup>(PointsPickupClass, SpawnLocation, SpawnRotation, params);
                    if(NewPoints){
                        NewPoints->SetReplicates(true);
                    }
                }
            }
        }

        AAITowerController* TowerAI = Cast<AAITowerController>(GetController());
        if (TowerAI) {
            TowerAI->CurrentState = TowerAI->Dead;
        }
        
        platform->Occupied = false;
        
        GetWorldTimerManager().SetTimer(this, &ATowerPawn::DeadTower, 0.25f, true);
        Controller->UnPossess();
        Destroy();
    }
}

void ATowerPawn::HurtPlayer()
{
    UGameplayStatics::GetPlayerPawn(this, 0)->TakeDamage(TowerDamage, FDamageEvent(), GetInstigatorController(), this);
}

bool ATowerPawn::RotateToFace_Validate() { return true; }
void ATowerPawn::RotateToFace_Implementation()
{
    
    APawn* CharacterLocation = NULL;
    AGodsAndMonstersGameMode* GM = GetWorld()->GetAuthGameMode<AGodsAndMonstersGameMode>();
    if(GM){
        TArray<AFPSCharacterController*> MonsterList = GM->MonsterList;
        for(AFPSCharacterController* MonsterController : MonsterList){
            if(MonsterController->GetTeamNum() != TeamNum){
                CharacterLocation = MonsterController->GetPawn();
            }
        }
    }

    if(CharacterLocation){
        FVector PointOfFocus = CharacterLocation->GetActorLocation();
        
        FVector NewFacing = (PointOfFocus - GetActorLocation());
        NewFacing = FVector(NewFacing.X, NewFacing.Y, 0);
        NewFacing.Normalize();
        
        FRotator NewRotation = FRotator(0, NewFacing.Rotation().Yaw, 0);
        
        SetActorRotation(NewRotation);
        
        CurrentFacing = NewRotation.Vector();
    }
}

bool ATowerPawn::FireAtMonster_Validate(){return true;}
void ATowerPawn::FireAtMonster_Implementation()
{
    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("FireAtMonster Called."));
        // [Server] try and fire a projectile
        if(Role == ROLE_Authority){
            if(ProjectileClass != NULL) {
                const FRotator SpawnRotation = GetControlRotation();
                // MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
                const FVector SpawnLocation = GetActorLocation() + SpawnRotation.RotateVector(GunOffset);
                FActorSpawnParameters params;
                params.Owner = this;
                params.Instigator = Instigator;
                UWorld* const World = GetWorld();
                if(World != NULL){
                    // spawn the projectile at the muzzle
                    AProjectile* NewProjectile = World->SpawnActor<AProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, params);
                    if(!NewProjectile){
                        return;
                    }
                    NewProjectile->SetReplicates(true);
                    NewProjectile->TeamNum = TeamNum;
                }
            }
        }
     

}

void ATowerPawn::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>&
                                                 OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    
    DOREPLIFETIME( ATowerPawn, HP);
    DOREPLIFETIME( ATowerPawn, CurrentFacing )
}

