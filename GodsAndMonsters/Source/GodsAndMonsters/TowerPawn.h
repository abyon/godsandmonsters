// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "Projectile.h"
#include "Placeable.h"
#include "PointsPickup.h"
#include "TowerPawn.generated.h"

UCLASS()
class GODSANDMONSTERS_API ATowerPawn : public APlaceable
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATowerPawn();
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category=Movement)
    FVector CurrentFacing;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Tower)
    bool bIsActive;
    

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
    
    
    //UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    //UAnimMontage* AttackAnim;
    
    //UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
    //UAnimMontage* DeathAnim;
    
    UFUNCTION(Reliable, NetMulticast, WithValidation)
    void DeadTower();
    bool DeadTower_Validate();
    void DeadTower_Implementation();
    
    UFUNCTION(Reliable, NetMulticast, WithValidation)
    void RotateToFace();
    bool RotateToFace_Validate();
    void RotateToFace_Implementation();
    
    UFUNCTION(Reliable, NetMulticast, WithValidation)
    void StartAttack();
    bool StartAttack_Validate();
    void StartAttack_Implementation();
    
    UFUNCTION(Reliable, NetMulticast, WithValidation)
    void StopAttack();
    bool StopAttack_Validate();
    void StopAttack_Implementation();
    
    
    UFUNCTION(Reliable, NetMulticast, WithValidation)
    void FireAtMonster();
    bool FireAtMonster_Validate();
    void FireAtMonster_Implementation();
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
    FVector GunOffset;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileClass)
    float FiringRate;
    
    void HurtPlayer();
    
    float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
    
    UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category=Health)
    float HP;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Weapon)
    float TowerDamage;
    
    UPROPERTY(VisibleDefaultsOnly, Category = Tower)
    class UBoxComponent* BaseCollisionComponent;
    
    UPROPERTY(VisibleDefaultsOnly, Category = Tower)
    class USceneComponent* BaseDummyComponent;
    
    UPROPERTY(EditDefaultsOnly, Category = ProjectileClass)
    TSubclassOf<class AProjectile> ProjectileClass;
    
    /** Projectile class to spawn */
    UPROPERTY(EditDefaultsOnly, Category = PointsClass)
    TSubclassOf<class APointsPickup> PointsPickupClass;
    
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;
	
};
